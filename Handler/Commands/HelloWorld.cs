﻿using System;

namespace Handler.Commands {
    public class HelloWorld : CommandBase {
        public String name() {
            return "hello";
        }

        public string description() {
            return "Prints \"Hello, world!\" to the console!";
        }

        public void perform() {
            System.Console.WriteLine("Hello, world!");
        }
    }
}
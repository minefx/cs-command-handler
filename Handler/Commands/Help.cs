﻿using System;
using System.Collections.Generic;

namespace Handler.Commands {
    public class Help : CommandBase {
        public string name() {
            return "help";
        }

        public string description() {
            return "List of all the commands.";
        }

        public void perform() {
            List<CommandBase> commands = Program.commands;
            Console.WriteLine("=====Help=====");
            for (int i = 0; i < commands.Count; i++) {
                Console.WriteLine(commands[i].name() + "   " + commands[i].description());
            }
            Console.WriteLine("==============");
        }
    }
}
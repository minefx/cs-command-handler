﻿using System;

namespace Handler.Commands {
    public interface CommandBase {
        String name();
        String description();

        void perform();
    }
}
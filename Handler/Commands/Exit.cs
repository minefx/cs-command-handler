﻿using System;

namespace Handler.Commands {
    public class Exit : CommandBase {
        public string name() {
            return "exit";
        }

        public string description() {
            return "Exits the application";
        }

        public void perform() {
            Environment.Exit(1);
        }
    }
}
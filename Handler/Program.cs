﻿using System;
using System.Collections.Generic;
using Handler.Commands;

namespace Handler {
    internal class Program {
        public static List<CommandBase> commands = new List<CommandBase>();
        public static void Main(string[] args) {
            bool commandExists = false;
            Console.BackgroundColor = ConsoleColor.DarkBlue;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Clear();
            Console.Write("> ");
            commands.Add(new HelloWorld());
            commands.Add(new Exit());
            commands.Add(new Help());
            while (true) {
                String input = Console.ReadLine();
                commandExists = false;
                Console.Write("> ");
                for (int i = 0; i < commands.Count; i++) {
                    if (commands[i].name().ToLower() == input.ToLower()) {
                        commandExists = true;
                        commands[i].perform();
                    }
                    if (!commandExists) {
                        Console.WriteLine("Unknown Command!");
                    }
                }
            }
        }
    }
}